#pragma once

namespace Key_correct {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for KeyCorrect
	/// </summary>
	public ref class KeyCorrect : public System::Windows::Forms::Form
	{
	public:
		KeyCorrect(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~KeyCorrect()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	private: System::Windows::Forms::Button^  button_Run;
	private: System::Windows::Forms::Button^  button_Exit;









	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button_Run = (gcnew System::Windows::Forms::Button());
			this->button_Exit = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button_Run
			// 
			this->button_Run->Location = System::Drawing::Point(13, 192);
			this->button_Run->Name = L"button_Run";
			this->button_Run->Size = System::Drawing::Size(106, 23);
			this->button_Run->TabIndex = 2;
			this->button_Run->Text = L"Run";
			this->button_Run->UseVisualStyleBackColor = true;
			// 
			// button_Exit
			// 
			this->button_Exit->Location = System::Drawing::Point(314, 192);
			this->button_Exit->Name = L"button_Exit";
			this->button_Exit->Size = System::Drawing::Size(106, 23);
			this->button_Exit->TabIndex = 3;
			this->button_Exit->Text = L"Exit";
			this->button_Exit->UseVisualStyleBackColor = true;
			// 
			// KeyCorrect
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(429, 258);
			this->Controls->Add(this->button_Exit);
			this->Controls->Add(this->button_Run);
			this->Name = L"KeyCorrect";
			this->Text = L"KeyCorrect1";
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
